import Foundation

extension Date {
    
    func rssLocalized() -> String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "d MMM yyyy, HH:mm"
        dateFormatter.locale = Locale(identifier: Locale.preferredLanguages.first ?? "")
        
        return dateFormatter.string(from: self)
    }
    
}
