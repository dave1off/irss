import UIKit

final class ZeroPaddingTextView: UITextView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        textContainerInset = .zero
        layoutMargins = .zero
        textContainer.lineFragmentPadding = 0
    }
    
}
