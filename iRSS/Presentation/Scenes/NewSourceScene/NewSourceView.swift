import UIKit

protocol INewSourceView: class {
    
    func addSource(response: PresentationDTOs.AddSource.Response)
    
}

final class NewSourceView: UIViewController {
    
    private let fieldsStackView = UIStackView()
    private let buttonsStackView = UIStackView()
    
    private let titleField = UITextField()
    private let linkField = UITextField()
    private let addButton = UIButton(type: .system)
    private let cancelButton = UIButton(type: .system)
    
    var presenter: INewSourcePresenter!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        titleField.becomeFirstResponder()
    }

}

extension NewSourceView: INewSourceView {
    
    func addSource(response: PresentationDTOs.AddSource.Response) {
        dismiss(animated: true)
    }
    
}

private extension NewSourceView {
    
    func setupUI() {
        view.backgroundColor = .white
        
        titleField.placeholder = "Название источника"
        titleField.textAlignment = .center
        titleField.font = .systemFont(ofSize: 20)
        titleField.autocorrectionType = .no
        
        linkField.placeholder = "Cсылка на источник"
        linkField.textAlignment = .center
        linkField.font = .systemFont(ofSize: 20)
        linkField.autocorrectionType = .no
        
        addButton.setTitle("Добавить", for: .normal)
        addButton.titleLabel?.font = .systemFont(ofSize: 20)
        addButton.addTarget(self, action: #selector(addTapped(_:)), for: .touchUpInside)
        
        cancelButton.setTitle("Отменить", for: .normal)
        cancelButton.setTitleColor(.red, for: .normal)
        cancelButton.titleLabel?.font = .systemFont(ofSize: 20)
        cancelButton.addTarget(self, action: #selector(cancelTapped(_:)), for: .touchUpInside)
        
        fieldsStackView.axis = .vertical
        fieldsStackView.translatesAutoresizingMaskIntoConstraints = false
        fieldsStackView.addArrangedSubview(titleField)
        fieldsStackView.addArrangedSubview(linkField)
        fieldsStackView.spacing = 30
        view.addSubview(fieldsStackView)
        
        buttonsStackView.axis = .horizontal
        buttonsStackView.translatesAutoresizingMaskIntoConstraints = false
        buttonsStackView.addArrangedSubview(addButton)
        buttonsStackView.addArrangedSubview(cancelButton)
        buttonsStackView.spacing = 40
        view.addSubview(buttonsStackView)
        
        NSLayoutConstraint.activate([
            fieldsStackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50),
            fieldsStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            fieldsStackView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.9),
            
            buttonsStackView.topAnchor.constraint(equalTo: fieldsStackView.bottomAnchor, constant: 50),
            buttonsStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    @objc func cancelTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @objc func addTapped(_ sender: UIButton) {
        presenter.addSource(request: .init(title: titleField.text ?? "", link: linkField.text ?? ""))
    }
    
}
