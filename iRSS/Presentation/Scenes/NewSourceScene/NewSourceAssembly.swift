import UIKit

final class NewSourceAssembly: IAssembly {
    
    func build() -> UIViewController {
        let view = NewSourceView()
        
        let addSourceRepository = SourcesRepository(dbService: DBService.shared)
        let addSourceUseCase = AddSourceUseCase(addSourceRepository: addSourceRepository)
        let presenter = NewSourcePresenter(addSourceUseCase: addSourceUseCase, view: view)
        
        view.presenter = presenter
        
        return view
    }
    
}
