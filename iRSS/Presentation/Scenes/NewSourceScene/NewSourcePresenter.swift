import Foundation

protocol INewSourcePresenter {
    
    func addSource(request: PresentationDTOs.AddSource.Request)
    
}

final class NewSourcePresenter: INewSourcePresenter {
    
    private let addSourceUseCase: IAddSourceUseCase
    
    private weak var view: INewSourceView?
    
    init(addSourceUseCase: IAddSourceUseCase, view: INewSourceView) {
        self.addSourceUseCase = addSourceUseCase
        self.view = view
    }
    
    func addSource(request: PresentationDTOs.AddSource.Request) {
        addSourceUseCase.exec(input: .init(title: request.title, link: request.link)) { [weak self] response in
            guard response.error == nil else { return }
            
            DispatchQueue.main.async {
                self?.view?.addSource(response: .init())
            }
        }
    }
    
}
