import UIKit

protocol IPostsRouter {
    
    func postSelected(response: PostsDTOs.PostSelected.Response)
    
}

final class PostsRouter: IPostsRouter {
    
    private weak var view: UIViewController?
    
    init(view: UIViewController) {
        self.view = view
    }
    
    func postSelected(response: PostsDTOs.PostSelected.Response) {
        let post = response.post
        let postAssembly = PostAssembly(dto: .init(post: .init(title: post.title, date: post.date, description: post.description)))
        view?.navigationController?.pushViewController(postAssembly.build(), animated: true)
    }
    
}
