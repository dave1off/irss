import Foundation

protocol IPostsPresenter {
    
    func onViewDidLoad(request: PostsDTOs.ViewDidLoad.Request)
    func onPostSelected(request: PostsDTOs.PostSelected.Request)
    func onUpdate(request: PostsDTOs.ViewDidLoad.Request)
    
}

final class PostsPresenter: IPostsPresenter {
    
    private weak var view: IPostsView?
    
    private let source: PostsDTOs.Init.Source
    
    private let getPostsUseCase: IGetPostsUseCase
    private let readPostUseCase: IReadPostUseCase
    
    private var posts: [PresentationDomainDTOs.GetPostsUseCase.Response.Post] = []
    
    init(dto: PostsDTOs.Init, getPostsUseCase: IGetPostsUseCase, readPostUseCase: IReadPostUseCase, view: IPostsView) {
        source = dto.source
        
        self.view = view
        self.getPostsUseCase = getPostsUseCase
        self.readPostUseCase = readPostUseCase
    }
    
    func onViewDidLoad(request: PostsDTOs.ViewDidLoad.Request) {
        view?.viewDidLoad(response: .init(title: source.title))
        
        getPostsUseCase.exec(request: .init(source: .init(title: source.title, link: source.link))) { [weak self] response in
            self?.refreshView(result: response.result)
        }
    }
    
    func onPostSelected(request: PostsDTOs.PostSelected.Request) {
        let selectedPost = posts[request.index]
        
        readPostUseCase.exec(request: .init(post: .init(link: selectedPost.link))) { [weak self] response in
            guard let self = self, case .success = response.result else { return }
            
            let post = PostsDTOs.PostSelected.Response.Post(
                title: selectedPost.title, date: selectedPost.date, description: selectedPost.description
            )
            
            DispatchQueue.main.async {
                self.view?.postSelected(response: .init(post: post))
            }
        }
    }
    
    func onUpdate(request: PostsDTOs.ViewDidLoad.Request) {
        view?.viewDidLoad(response: .init(title: source.title))
        
        getPostsUseCase.exec(request: .init(source: .init(title: source.title, link: source.link))) { [weak self] response in
            self?.refreshView(result: response.result)
        }
    }
    
}

private extension PostsPresenter {
    
    func refreshView(result: Result<[PresentationDomainDTOs.GetPostsUseCase.Response.Post]>) {
        guard case .success(let posts) = result else { return }
        
        self.posts = posts
        let toView = self.posts.map { PostsDTOs.UpdatePosts.Response.Post(title: $0.title, date: $0.date, isRead: $0.isRead) }
        
        DispatchQueue.main.async {
            self.view?.viewDidLoad(response: .init(posts: toView))
        }
    }
    
}
