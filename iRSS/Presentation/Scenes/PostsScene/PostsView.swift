import UIKit

protocol IPostsView: class {
    
    func viewDidLoad(response: PostsDTOs.LoadSource.Response)
    func viewDidLoad(response: PostsDTOs.UpdatePosts.Response)
    func postSelected(response: PostsDTOs.PostSelected.Response)
    
}

final class PostsView: UIViewController {
    
    private let postsTableView = UITableView()
    
    private var posts: [PostsDTOs.UpdatePosts.Response.Post] = [] {
        didSet {
            postsTableView.reloadData()
        }
    }
    
    var presenter: IPostsPresenter!
    var router: IPostsRouter!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        presenter.onViewDidLoad(request: .init())
    }

}

extension PostsView: IPostsView {
    
    func viewDidLoad(response: PostsDTOs.UpdatePosts.Response) {
        postsTableView.refreshControl?.endRefreshing()
        posts = response.posts
    }
    
    func viewDidLoad(response: PostsDTOs.LoadSource.Response) {
        navigationItem.title = response.title
    }
    
    func postSelected(response: PostsDTOs.PostSelected.Response) {
        router.postSelected(response: response)
    }
    
}

private extension PostsView {
    
    func setupUI() {
        view.backgroundColor = .white
        
        postsTableView.delegate = self
        postsTableView.dataSource = self
        postsTableView.tableFooterView = UIView()
        postsTableView.translatesAutoresizingMaskIntoConstraints = false
        postsTableView.estimatedRowHeight = 80
        postsTableView.rowHeight = UITableView.automaticDimension
        postsTableView.register(SourceTableCell.self, forCellReuseIdentifier: "Cell")
        postsTableView.separatorInset = .zero
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(onUpdate(_:)), for: .valueChanged)
        
        postsTableView.refreshControl = refreshControl
        view.addSubview(postsTableView)
        
        NSLayoutConstraint.activate([
            postsTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            postsTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            postsTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            postsTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)
        ])
    }
    
    @objc func onUpdate(_ sender: UIRefreshControl) {
        presenter.onUpdate(request: .init())
    }
    
}

extension PostsView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.onPostSelected(request: .init(index: indexPath.row))
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension PostsView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! SourceTableCell
        let post = posts[indexPath.row]
        
        cell.update(with: .init(title: post.title, link: post.date.rssLocalized(), isRead: post.isRead))
        
        return cell
    }
    
}
