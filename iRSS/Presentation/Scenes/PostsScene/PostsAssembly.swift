import UIKit

final class PostsAssembly: IAssembly {
    
    private let dto: PostsDTOs.Init
    
    init(dto: PostsDTOs.Init) {
        self.dto = dto
    }
    
    func build() -> UIViewController {
        let view = PostsView()
        
        let postsRepository = PostsRepository(dbService: DBService.shared)
        let sourceRepository = SourcesRepository(dbService: DBService.shared)
        
        let getPostsFromDBUseCase = GetPostsFromDBUseCase(getPostsFromDBRepository: postsRepository)
        let getPostsFromNetworkUseCase = GetPostsFromNetworkUseCase()
        let readPostUseCase = ReadPostUseCase(readPostRepository: postsRepository)
        let refreshSourceWithPostsUseCase = RefreshSourceWithPostsUseCase(refreshSourceWithPostsRepository: sourceRepository)
        
        let getPostsUseCase = GetPostsUseCase(
            getPostsFromDBUseCase: getPostsFromDBUseCase,
            getPostsFromNetworkUseCase: getPostsFromNetworkUseCase,
            refreshSourceWithPostsUseCase: refreshSourceWithPostsUseCase
        )
        
        let presenter = PostsPresenter(dto: dto, getPostsUseCase: getPostsUseCase, readPostUseCase: readPostUseCase, view: view)
        let router = PostsRouter(view: view)
        
        view.presenter = presenter
        view.router = router
        
        
        /* let sourcesRepository = SourcesRepository(dbService: DBService.shared)
        let getSourcesUseCase = GetSourcesUseCase(getSourcesRepository: sourcesRepository)
        
        let updateSourceObserver = UpdateSourcesObserver(
            updateSourcesObservable: SourcesObservable(context: DBService.shared.context)
        )
        
        let presenter = SourcesPresenter(
            getSourcesUseCase: getSourcesUseCase,
            updateSourceObserver: updateSourceObserver,
            view: view
        )
        
        view.presenter = presenter
        
        updateSourceObserver.onUpdate = { [weak presenter] result in
            presenter?.updateSourcesInView(from: result)
        } */
        
        return view
    }
    
}
