import Foundation

struct PostsDTOs {
    
    struct Init {
        
        struct Source {
            
            let title: String
            let link: String
            
        }
        
        let source: Source
        
    }
    
    struct ViewDidLoad {
        
        struct Request {
            
            
            
        }
        
        struct Response {
            
            
            
        }
        
    }
    
    struct LoadSource {
        
        struct Request { }
        
        struct Response {
            
            let title: String
            
        }
        
    }
    
    struct UpdatePosts {
        
        struct Request { }
        
        struct Response {
            
            struct Post {
                
                let title: String
                let date: Date
                let isRead: Bool
                
            }
            
            let posts: [Post]
            
        }
        
    }
    
    struct PostSelected {
        
        struct Request {
            
            let index: Int
            
        }
        
        struct Response {
            
            struct Post {
                
                let title: String
                let date: Date
                let description: String
                
            }
            
            let post: Post
            
        }
        
    }
    
}
