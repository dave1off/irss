import UIKit

protocol IMainNavigationView: class {
    
    func setViews(assemblies: [IAssembly])
    
}

final class MainNavigationView: UINavigationController {
    
    var presenter: IMainNavigationPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.onViewDidLoad()
    }
    
}

extension MainNavigationView: IMainNavigationView {
    
    func setViews(assemblies: [IAssembly]) {
        setViewControllers(assemblies.map { $0.build() }, animated: false)
    }
    
}
