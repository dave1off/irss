import Foundation

protocol IMainNavigationPresenter {
    
    func onViewDidLoad()
    
}

final class MainNavigationPresenter: IMainNavigationPresenter {
    
    private weak var view: IMainNavigationView?
    
    init(view: IMainNavigationView) {
        self.view = view
    }
    
    func onViewDidLoad() {
        view?.setViews(assemblies: [SourcesAssembly()])
    }
    
}
