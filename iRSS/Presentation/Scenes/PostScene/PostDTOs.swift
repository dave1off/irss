import Foundation

struct PostDTOs {
    
    struct Init {
        
        struct Post {
            
            let title: String
            let date: Date
            let description: String
            
        }
        
        let post: Post
        
    }
    
    struct ViewDidAppear {
        
        struct Request { }
        
        struct Response {
            
            struct ViewModel {
                
                let title: String
                let date: Date
                let description: String
                
            }
            
            let viewModel: ViewModel
            
        }
        
    }
    
}
