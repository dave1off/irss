import Foundation

protocol IPostPresenter {
    
    func onViewDidAppear(request: PostDTOs.ViewDidAppear.Request)
    
}

final class PostPresenter: IPostPresenter {
    
    private let dto: PostDTOs.Init
    
    private weak var view: IPostView?
    
    init(dto: PostDTOs.Init, view: IPostView) {
        self.dto = dto
        self.view = view
    }
    
    func onViewDidAppear(request: PostDTOs.ViewDidAppear.Request) {
        let post = dto.post
        
        view?.viewDidAppear(response: .init(viewModel: .init(title: post.title, date: post.date, description: post.description)))
    }
    
}
