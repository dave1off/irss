import UIKit

final class PostAssembly: IAssembly {
    
    private let dto: PostDTOs.Init
    
    init(dto: PostDTOs.Init) {
        self.dto = dto
    }
    
    func build() -> UIViewController {
        let view = PostView()
        let presenter = PostPresenter(dto: dto, view: view)
        
        view.presenter = presenter
        
        return view
    }
    
}
