import UIKit

protocol IPostView: class {
    
    func viewDidAppear(response: PostDTOs.ViewDidAppear.Response)
    
}

final class PostView: UIViewController {
    
    private let scrollView = UIScrollView()
    
    private let titleLabel = UILabel()
    private let dateLabel = UILabel()
    private let contentView = ZeroPaddingTextView()
    
    var presenter: IPostPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        presenter.onViewDidAppear(request: .init())
    }

}

extension PostView: IPostView {
    
    func viewDidAppear(response: PostDTOs.ViewDidAppear.Response) {
        var postDescription = response.viewModel.description
        
        titleLabel.text = response.viewModel.title
        dateLabel.text = response.viewModel.date.rssLocalized()
        
        ///
        
        let htmlContent = """
            <!DOCTYPE html>
            <html>
                <head>
                    <meta charset=utf-8>
        
                    <style>
                        * {
                            padding: 0;
                            margin: 0;
                            border: 0
                        }
                        
                        body {
                            font-family: Helvetica;
                            font-size: 15px;
                        }
                    </style>
                </head>
                <body>
                    \(response.viewModel.description.replacingOccurrences(of: "<br>", with: ""))
                </body>
            </html>
        """
        
        guard let data = htmlContent.data(using: .utf8) else { return }
        guard let attr = try? NSAttributedString(
            data: data,
            options: [
                .documentType: NSAttributedString.DocumentType.html
            ],
            documentAttributes: nil
        ) else { return }
        
        contentView.attributedText = attr
        
        guard let regex = try? NSRegularExpression(pattern: "<p>", options: .caseInsensitive) else { return }
        let range = NSRange(location: 0, length: postDescription.count)
        let matches = regex.numberOfMatches(
            in: postDescription,
            options: [],
            range: range
        )
        
        for _ in 1...matches {
            postDescription += "\n"
        }
        
        let contentViewHeight = postDescription.height(
            withConstrainedWidth: contentView.bounds.width,
            font: UIFont(name: "Helvetica", size: 15) ?? .systemFont(ofSize: 12)
        )
        
        let contentHeight = CGFloat.titleLabelTopMargin +
            titleLabel.intrinsicContentSize.height +
            CGFloat.dateLabelTopMargin +
            dateLabel.intrinsicContentSize.height +
            CGFloat.contentViewTopMargin +
            contentViewHeight +
            CGFloat.contentViewBottomMargin
        
        contentView.heightAnchor.constraint(equalToConstant: contentViewHeight).isActive = true
        
        let height = max(contentHeight, scrollView.bounds.height)
        scrollView.contentSize = CGSize(width: scrollView.bounds.width, height: height)
    }
    
}

private extension PostView {
    
    func setupUI() {
        view.backgroundColor = .white
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        view.addSubview(scrollView)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.font = .boldSystemFont(ofSize: 17)
        scrollView.addSubview(titleLabel)
        
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.textAlignment = .center
        dateLabel.font = .italicSystemFont(ofSize: 15)
        scrollView.addSubview(dateLabel)
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.isScrollEnabled = false
        contentView.isEditable = false
        scrollView.addSubview(contentView)
        
        NSLayoutConstraint.activate([
            scrollView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8),
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: .titleLabelTopMargin),
            titleLabel.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            titleLabel.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            
            dateLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .dateLabelTopMargin),
            dateLabel.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            dateLabel.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            
            contentView.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: .contentViewTopMargin),
            contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            contentView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
        ])
    }
    
}

fileprivate extension CGFloat {
    
    static let titleLabelTopMargin: CGFloat = 20
    static let dateLabelTopMargin: CGFloat = 20
    static let contentViewTopMargin: CGFloat = 20
    static let contentViewBottomMargin: CGFloat = 20
    
}
