import UIKit

final class SourceTableCell: UITableViewCell {
    
    struct ViewModel {
        
        let title: String
        let link: String
        let isRead: Bool
        
    }
    
    private let stackView = UIStackView()
    private let titleLabel = UILabel()
    private let linkLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        titleLabel.numberOfLines = 1
        linkLabel.numberOfLines = 1
        linkLabel.font = .systemFont(ofSize: 15)
        linkLabel.textColor = .gray
        
        stackView.axis = .vertical
        stackView.spacing = 5
        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(linkLabel)
        
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            stackView.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
            
            titleLabel.heightAnchor.constraint(equalToConstant: 30),
            //linkLabel.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    
    func update(with viewModel: ViewModel) {
        titleLabel.text = viewModel.title
        linkLabel.text = viewModel.link
        
        if viewModel.isRead {
            backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        } else {
            backgroundColor = .white
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
