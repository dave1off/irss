import Foundation

struct SourcesDTOs {
    
    struct Init { }
    
    struct ViewDidLoad {
        
        struct Request { }
        
        struct Response {
            
            struct Post {
                
                let title: String
                let link: String
                
            }
            
            let posts: [Post]
            
        }
        
    }
    
    struct SourceSelected {
        
        struct Request {
            
            let index: Int
            
        }
        
        struct Response {
            
            struct Source {
                
                let title: String
                let link: String
                
            }
            
            let source: Source
            
        }
        
    }
    
}
