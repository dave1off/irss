import Foundation

protocol ISourcesPresenter {
    
    func onViewDidLoad(request: SourcesDTOs.ViewDidLoad.Request)
    func onSourceSelected(request: SourcesDTOs.SourceSelected.Request)
    
}

final class SourcesPresenter: ISourcesPresenter {
    
    private weak var view: ISourcesView?
    
    private let getSourcesUseCase: IGetSourcesUseCase
    private let updateSourceObserver: IUpdateSourcesObserver
    
    private var sources: [PresentationDomainDTOs.GetSourcesUseCase.Response.Item] = []
    
    init(getSourcesUseCase: IGetSourcesUseCase, updateSourceObserver: IUpdateSourcesObserver, view: ISourcesView) {
        self.getSourcesUseCase = getSourcesUseCase
        self.updateSourceObserver = updateSourceObserver
        self.view = view
    }
    
    func onViewDidLoad(request: SourcesDTOs.ViewDidLoad.Request) {
        getSourcesUseCase.exec(request: .init()) { [weak self] response in
            self?.updateSourcesInView(from: response)
        }
    }
    
    func updateSourcesInView(from response: PresentationDomainDTOs.GetSourcesUseCase.Response) {
        switch response.result {
        case .success(let items):
            sources = items
            
            DispatchQueue.main.async { [weak self] in
                let sources = items.map { SourcesDTOs.ViewDidLoad.Response.Post(title: $0.title, link: $0.link) }
                self?.view?.viewDidLoad(response: .init(posts: sources))
            }
        case .failure:
            break
        }
    }
    
    func onSourceSelected(request: SourcesDTOs.SourceSelected.Request) {
        let source = sources[request.index]
        view?.sourceSelected(response: .init(source: .init(title: source.title, link: source.link)))
    }
    
}
