import UIKit

protocol ISourcesView: class {
    
    func sourceSelected(response: SourcesDTOs.SourceSelected.Response)
    func viewDidLoad(response: SourcesDTOs.ViewDidLoad.Response)
    
}

class SourcesView: UIViewController {
    
    private let sourcesTableView = UITableView()
    
    private var sources: [SourcesDTOs.ViewDidLoad.Response.Post] = [] {
        didSet {
            sourcesTableView.reloadData()
        }
    }
    
    var presenter: ISourcesPresenter!
    var router: ISourcesRouter!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        presenter.onViewDidLoad(request: .init())
    }

}

extension SourcesView: ISourcesView {
    
    func sourceSelected(response: SourcesDTOs.SourceSelected.Response) {
        router.sourceSelected(response: response)
    }
    
    func viewDidLoad(response: SourcesDTOs.ViewDidLoad.Response) {
        sources = response.posts
    }
    
}

private extension SourcesView {
    
    func setupUI() {
        navigationItem.title = "Источники"
        
        view.backgroundColor = .white
        
        sourcesTableView.delegate = self
        sourcesTableView.dataSource = self
        sourcesTableView.tableFooterView = UIView()
        sourcesTableView.translatesAutoresizingMaskIntoConstraints = false
        sourcesTableView.estimatedRowHeight = 80
        sourcesTableView.rowHeight = UITableView.automaticDimension
        sourcesTableView.register(SourceTableCell.self, forCellReuseIdentifier: "Cell")
        sourcesTableView.separatorInset = .zero
        view.addSubview(sourcesTableView)
        
        let addSourceItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onAddSourceTapped(_:)))
        navigationItem.rightBarButtonItem = addSourceItem
        
        NSLayoutConstraint.activate([
            sourcesTableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            sourcesTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            sourcesTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            sourcesTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)
        ])
    }
    
    @objc func onAddSourceTapped(_ sender: UIBarButtonItem) {
        present(NewSourceAssembly().build(), animated: true)
    }
    
}

extension SourcesView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.onSourceSelected(request: .init(index: indexPath.row))
        //tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension SourcesView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! SourceTableCell
        let source = sources[indexPath.row]
        
        cell.update(with: .init(title: source.title, link: source.link, isRead: false))
        
        return cell
    }
    
}
