import UIKit

final class SourcesAssembly: IAssembly {
    
    func build() -> UIViewController {
        let view = SourcesView()
        
        let router = SourcesRouter(view: view)
        
        let sourcesRepository = SourcesRepository(dbService: DBService.shared)
        let getSourcesUseCase = GetSourcesUseCase(getSourcesRepository: sourcesRepository)
        
        let updateSourceObserver = UpdateSourcesObserver(
            updateSourcesObservable: SourcesObservable(dbService: DBService.shared)
        )
        
        let presenter = SourcesPresenter(
            getSourcesUseCase: getSourcesUseCase,
            updateSourceObserver: updateSourceObserver,
            view: view
        )
        
        view.presenter = presenter
        view.router = router
        
        updateSourceObserver.onUpdate = { [weak presenter] result in
            presenter?.updateSourcesInView(from: result)
        }
        
        return view
    }
    
}
