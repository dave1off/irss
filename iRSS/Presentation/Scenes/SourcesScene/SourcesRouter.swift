import UIKit

protocol ISourcesRouter {
    
    func sourceSelected(response: SourcesDTOs.SourceSelected.Response)
    
}

final class SourcesRouter: ISourcesRouter {
    
    private weak var view: UIViewController?
    
    init(view: UIViewController) {
        self.view = view
    }
    
    func sourceSelected(response: SourcesDTOs.SourceSelected.Response) {
        let postsAssembly = PostsAssembly(dto: .init(source: .init(title: response.source.title, link: response.source.link)))
        
        view?.navigationController?.pushViewController(postsAssembly.build(), animated: true)
    }
    
}
