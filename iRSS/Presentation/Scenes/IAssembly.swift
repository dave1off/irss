import UIKit

protocol IAssembly {
    
    func build() -> UIViewController
    
}
