import Foundation
import CoreData

final class SourcesObservable: NSObject {
    
    private let fetchedResultController: NSFetchedResultsController<DBSourceModel>
    
    private weak var updateSourcesObserver: IUpdateSourcesObserver?
    
    init(dbService: IDBService) {
        let fetchRequest = NSFetchRequest<DBSourceModel>(entityName: DBSourceModel.entityName)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        
        fetchedResultController = dbService.createFetchedResultsController(fetchRequest: fetchRequest)
        
        super.init()
        
        fetchedResultController.delegate = self
        try? fetchedResultController.performFetch()
    }
    
}

extension SourcesObservable: NSFetchedResultsControllerDelegate {
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        let objects = (controller.fetchedObjects as? [DBSourceModel]) ?? []
        let items = objects.map { DomainDataDTOs.UpdateSources.Response.Item(title: $0.title, link: $0.link) }
        
        updateSourcesObserver?.updateSources(response: .init(result: items))
    }
    
}

extension SourcesObservable: ISourcesObservable {
    
    func addUpdateSourcesObserver(request: DomainDataDTOs.UpdateSources.Request) {
        updateSourcesObserver = request.observer
    }
    
}
