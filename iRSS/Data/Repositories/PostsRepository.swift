import Foundation
import CoreData

final class PostsRepository {
    
    private let dbService: IDBService
    
    init(dbService: IDBService) {
        self.dbService = dbService
    }
    
}

extension PostsRepository: IGetPostsFromDBRepository {
    
    func exec(request: DTO.Request, completion: @escaping (DTO.Response) -> Void) {
        let fetchRequest = NSFetchRequest<DBPostModel>(entityName: "DBPostModel")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        fetchRequest.predicate = NSPredicate(format: "source.link = %@", request.source.link)
        
        dbService.fetch(fetchRequest: fetchRequest) { response in
            guard case let .success(dbPosts) = response else { return }
            
            let result = dbPosts.map {
                DTO.Response.Post(link: $0.link, title: $0.title, description: $0.desc, date: $0.date, isRead: $0.isRead)
            }
            
            completion(.init(result: .success(result)))
        }
    }
    
}

extension PostsRepository: IReadPostRepository {
    
    func exec(request: ReadPostDTO.Request, completion: @escaping (ReadPostDTO.Response) -> Void) {
        let fetchRequest = NSFetchRequest<DBPostModel>(entityName: "DBPostModel")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
        fetchRequest.predicate = NSPredicate(format: "link = %@", request.post.link)
        
        dbService.update(
            fetchRequest: fetchRequest,
            modifications: { result in
                guard case let .success(dbPosts) = result, dbPosts.count == 1, let dbPost = dbPosts.first else { return }
                
                dbPost.isRead = true
            },
            completion:{ result in
                completion(.init(result: result))
            }
        )
        
        /* dbService.fetch(fetchRequest: fetchRequest) { [weak self] response in
            guard let self = self else { return }
            guard case let .success(dbPosts) = response, dbPosts.count == 1, let dbPost = dbPosts.first else { return }
            
            dbPost.isRead = true
            
            self.dbService.save { error in
                if let error = error {
                    completion(.init(result: .failure(error)))
                } else {
                    completion(.init(result: .success(())))
                }
            }
        } */
    }
    
}
