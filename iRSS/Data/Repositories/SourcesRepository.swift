import Foundation
import CoreData

final class SourcesRepository {
    
    private let dbService: IDBService
    
    private let dispatchGroup = DispatchGroup()
    
    init(dbService: IDBService) {
        self.dbService = dbService
    }
    
}

extension SourcesRepository: IAddSourceRepository {
    
    func exec(request: DomainDataDTOs.AddSource.Request, completion: @escaping (DomainDataDTOs.AddSource.Response) -> Void) {
        dbService.new(
            DBSourceModel.self,
            modifications: { newObject in
                newObject.title = request.title
                newObject.link = request.link
            }, completion: { result in
                completion(.init(result: result))
            }
        )
    }
    
}

extension SourcesRepository: IGetSourcesRepository {
    
    func exec(request: DomainDataDTOs.GetSources.Request, completion: @escaping (DomainDataDTOs.GetSources.Response) -> Void) {
        let fetchRequest = NSFetchRequest<DBSourceModel>(entityName: "DBSourceModel")
        
        dbService.fetch(fetchRequest: fetchRequest) { result in
            switch result {
            case .success(let elements):
                let items = elements.map { DomainDataDTOs.GetSources.Response.Item(title: $0.title, link: $0.link) }
                completion(.init(result: .success(items)))
            case .failure(let error):
                completion(.init(result: .failure(error)))
            }
        }
    }
    
}

extension SourcesRepository: IRefreshSourceWithPostsRepository {
    
    func exec(request: DTO.Request, completion: @escaping (DTO.Response) -> Void) {
        let fetchRequest = NSFetchRequest<DBSourceModel>(entityName: "DBSourceModel")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "title", ascending: true)]
        fetchRequest.predicate = NSPredicate(format: "link = %@", request.source.link)
        
        dbService.fetch(fetchRequest: fetchRequest) { [weak self] response in
            guard let self = self else { return }
            
            switch response {
            case .success(let sources):
                guard sources.count == 1, let source = sources.first else { return }
                
                source.title = request.source.title
                source.link = request.source.link
                
                self.dbService.delete(objects: Array(source.posts)) { result in
                    guard case .success = result else { return }
                    
                    source.posts = []
                    
                    for post in request.source.posts {
                        self.dispatchGroup.enter()
                        
                        self.dbService.new(
                            DBPostModel.self,
                            modifications: { newObject in
                                newObject.date = post.date
                                newObject.title = post.title
                                newObject.isRead = post.isRead
                                newObject.desc = post.description
                                newObject.link = post.link
                                
                                source.posts.insert(newObject)
                            }, completion: { result in
                                guard case .success = result else { return }
                                
                                self.dispatchGroup.leave()
                            }
                        )
                    }
                    
                    self.dispatchGroup.notify(queue: .global()) {
                        completion(.init(result: .success(())))
                    }
                }
            case .failure(let error):
                completion(.init(result: .failure(error)))
            }
        }
    }
    
}
