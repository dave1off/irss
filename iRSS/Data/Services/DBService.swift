import Foundation
import CoreData

protocol IDBService {
    
    func fetch<T>(fetchRequest: NSFetchRequest<T>, completion: @escaping (Result<[T]>) -> Void) where T: NSManagedObject
    func delete<T>(objects: [T], completion: @escaping (Result<Void>) -> ()) where T: NSManagedObject
    
    func new<T>(
        _ entity: T.Type,
        modifications: @escaping (T) -> Void,
        completion: @escaping (Result<Void>) -> Void
    ) where T: NSManagedObject
    
    func update<T>(
        fetchRequest: NSFetchRequest<T>,
        modifications: @escaping (Result<[T]>) -> Void,
        completion: @escaping (Result<Void>) -> Void
    ) where T: NSManagedObject
    
    func createFetchedResultsController<T>(fetchRequest: NSFetchRequest<T>) -> NSFetchedResultsController<T> where T: NSManagedObject
    
}

final class DBService: IDBService {
    
    static let shared = DBService()
    
    private init() { }
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "RSS")
        
        container.loadPersistentStores { _, _ in }
        
        return container
    }()
    
    private lazy var context = persistentContainer.newBackgroundContext()
    
    func new<T>(
        _ entity: T.Type,
        modifications: @escaping (T) -> Void,
        completion: @escaping (Result<Void>) -> Void
    ) where T: NSManagedObject {
        context.perform { [weak self] in
            guard let self = self else { return }
            
            let entity = NSEntityDescription.insertNewObject(forEntityName: T.entityName, into: self.context) as! T
            modifications(entity)
            
            do {
                try self.context.save()
            } catch {
                completion(.failure(error))
            }
            
            completion(.success(()))
        }
    }
    
    func update<T>(
        fetchRequest: NSFetchRequest<T>,
        modifications: @escaping (Result<[T]>) -> Void,
        completion: @escaping (Result<Void>) -> Void
    ) where T: NSManagedObject {
        context.perform { [weak self] in
            guard let self = self else { return }
            
            do {
                let objects = try self.context.fetch(fetchRequest)
                modifications(.success(objects))
                
                try self.context.save()
            } catch {
                completion(.failure(error))
            }
            
            completion(.success(()))
        }
    }
    
    func fetch<T>(fetchRequest: NSFetchRequest<T>, completion: @escaping (Result<[T]>) -> Void) where T: NSManagedObject {
        context.perform { [weak self] in
            guard let self = self else { return }
            
            do {
                let elements = try self.context.fetch(fetchRequest)
                completion(.success(elements))
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    func delete<T>(objects: [T], completion: @escaping (Result<Void>) -> ()) where T: NSManagedObject {
        context.perform { [weak self] in
            guard let self = self else { return }
            
            for object in objects {
                self.context.delete(object)
            }
            
            do {
                try self.context.save()
            } catch {
                completion(.failure(error))
            }
            
            completion(.success(()))
        }
    }
    
    func createFetchedResultsController<T>(fetchRequest: NSFetchRequest<T>) -> NSFetchedResultsController<T> where T: NSManagedObject {
        return NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
    }
    
}
