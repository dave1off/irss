import CoreData

extension NSManagedObject {
    
    static var entityName: String {
        return description().components(separatedBy: ".").last ?? ""
    }
    
}
