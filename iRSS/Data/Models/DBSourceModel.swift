import Foundation
import CoreData

@objc(DBSourceModel)
public class DBSourceModel: NSManagedObject {
    
    @NSManaged public var title: String
    @NSManaged public var link: String
    @NSManaged public var posts: Set<DBPostModel>
    
}

