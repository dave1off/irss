import Foundation
import CoreData

@objc(DBPostModel)
public class DBPostModel: NSManagedObject {
    
    @NSManaged public var title: String
    @NSManaged public var link: String
    @NSManaged public var desc: String
    @NSManaged public var date: Date
    @NSManaged public var isRead: Bool
    
    @NSManaged public var source: DBSourceModel
    
}

