import Foundation

protocol IUpdateSourcesObserver: class {
    
    func updateSources(response: DomainDataDTOs.UpdateSources.Response)
    
}
