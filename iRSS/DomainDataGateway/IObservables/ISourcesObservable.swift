import Foundation

protocol ISourcesObservable {
    
    func addUpdateSourcesObserver(request: DomainDataDTOs.UpdateSources.Request)
    
}
