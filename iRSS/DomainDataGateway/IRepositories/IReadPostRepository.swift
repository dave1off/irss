import Foundation

protocol IReadPostRepository {
    
    typealias ReadPostDTO = DomainDataDTOs.ReadPostRepository
    
    func exec(request: ReadPostDTO.Request, completion: @escaping (ReadPostDTO.Response) -> Void)
    
}
