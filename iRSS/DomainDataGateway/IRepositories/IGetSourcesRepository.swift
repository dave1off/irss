import Foundation

protocol IGetSourcesRepository {
    
    typealias GetSources = DomainDataDTOs.GetSources
    
    func exec(request: GetSources.Request, completion: @escaping (GetSources.Response) -> Void)
    
}
