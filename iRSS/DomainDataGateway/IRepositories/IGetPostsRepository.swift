import Foundation

protocol IGetPostsRepository {
    
    typealias GetPostsFromDB = DomainDataDTOs.GetPostsFromDB
    
    func exec(request: GetPostsFromDB.Request, completion: @escaping (GetPostsFromDB.Response) -> Void)
    
}
