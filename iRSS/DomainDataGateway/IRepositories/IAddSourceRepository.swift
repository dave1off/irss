import Foundation

protocol IAddSourceRepository {
    
    typealias AddSource = DomainDataDTOs.AddSource
    
    func exec(request: AddSource.Request, completion: @escaping (AddSource.Response) -> Void)
    
}
