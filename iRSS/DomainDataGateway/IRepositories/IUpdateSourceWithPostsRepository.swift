import Foundation

protocol IRefreshSourceWithPostsRepository {
    
    typealias DTO = DomainDataDTOs.RefreshSourceWithPostsRepository
    
    func exec(request: DTO.Request, completion: @escaping (DTO.Response) -> Void)
    
}
