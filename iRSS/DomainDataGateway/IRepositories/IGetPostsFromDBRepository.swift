import Foundation

protocol IGetPostsFromDBRepository {
    
    typealias DTO = DomainDataDTOs.GetPostsFromDBRepository
    
    func exec(request: DTO.Request, completion: @escaping (DTO.Response) -> Void)
    
}
