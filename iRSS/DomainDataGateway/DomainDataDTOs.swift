import Foundation

struct DomainDataDTOs {
    
    struct AddSource {
        
        struct Request {
            
            let title: String
            let link: String
            
        }
        
        struct Response {
            
            let result: Result<Void>
            
        }
        
    }
    
    struct GetSources {
        
        struct Request { }
        
        struct Response {
            
            struct Item {
                
                let title: String
                let link: String
                
            }
            
            let result: Result<[Item]>
            
        }
        
    }
    
    struct UpdateSources {
        
        struct Request {
            
            let observer: IUpdateSourcesObserver
            
        }
        
        struct Response {
            
            struct Item {
                
                let title: String
                let link: String
                
            }
            
            let result: [Item]
            
        }
        
    }
    
    struct GetPostsFromDB {
        
        struct Request {
            
            let link: String
            
        }
        
        struct Response {
            
            struct Post {
                
                let title: String
                let description: String
                let date: Date
                let link: String
                let isRead: Bool
                
            }
            
            let result: Result<[Post]>
            
        }
        
    }
    
    struct SavePostsToDB {
        
        struct Request {
            
            struct Source {
                
                let link: String
                
            }
            
            struct Post {
                
                let title: String
                let description: String
                let date: Date
                let link: String
                let isRead: Bool
                
            }
            
            let source: Source
            let posts: [Post]
            
        }
        
        struct Response {
            
            let result: Result<Void>
            
        }
        
    }
    
    struct RefreshSourceWithPostsRepository {
        
        struct Request {
            
            struct Source {
                
                let title: String
                let link: String
                let posts: [Post]
                
            }
            
            struct Post {
                
                let link: String
                let title: String
                let description: String
                let date: Date
                let isRead: Bool
                
            }
            
            let source: Source
            
        }
        
        struct Response {
            
            let result: Result<Void>
            
        }
        
    }
    
    struct GetPostsFromDBRepository {
        
        struct Request {
            
            struct Source {
                
                let link: String
                
            }
            
            let source: Source
            
        }
        
        struct Response {
            
            struct Post {
                
                let link: String
                let title: String
                let description: String
                let date: Date
                let isRead: Bool
                
            }
            
            let result: Result<[Post]>
            
        }
        
    }
    
    struct ReadPostRepository {
        
        struct Request {
            
            struct Post {
                
                let link: String
                
            }
            
            let post: Post
            
        }
        
        struct Response {
            
            let result: Result<Void>
            
        }
        
    }
    
}
