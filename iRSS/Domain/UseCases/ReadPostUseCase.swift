import Foundation

final class ReadPostUseCase: IReadPostUseCase {
    
    private let readPostRepository: IReadPostRepository
    
    init(readPostRepository: IReadPostRepository) {
        self.readPostRepository = readPostRepository
    }
    
    func exec(request: DTO.Request, completion: @escaping (DTO.Response) -> Void) {
        readPostRepository.exec(request: .init(post: .init(link: request.post.link))) { response in
            completion(.init(result: response.result))
        }
    }
    
}
