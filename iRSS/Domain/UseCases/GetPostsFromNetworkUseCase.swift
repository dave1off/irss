import Foundation

final class GetPostsFromNetworkUseCase: NSObject, IGetPostsFromNetworkUseCase {
    
    private var inItem = false
    private var postTitle = ""
    private var postLink = ""
    private var postDescription = ""
    private var postDate = Date()
    
    private var isTitle = false
    private var isDescription = false
    private var isDate = false
    private var isLink = false
    
    private var result: [DTO.Response.Post] = []
    
    func exec(request: DTO.Request, completion: @escaping (DTO.Response) -> Void) {
        guard let url = URL(string: request.source.link) else { return }
        
        refreshData()
        
        DispatchQueue.global().async { [weak self] in
            guard let self = self else { return }
            guard let parser = XMLParser(contentsOf: url) else { return }
            
            parser.delegate = self
            parser.parse()
            completion(.init(result: .success(self.result)))
        }
    }
    
}

private extension GetPostsFromNetworkUseCase {
    
    func refreshData() {
        inItem = false
        postTitle = ""
        postLink = ""
        postDescription = ""
        postDate = Date()
        
        isTitle = false
        isDescription = false
        isDate = false
        isLink = false
        
        result = []
    }
    
}

extension GetPostsFromNetworkUseCase: XMLParserDelegate {
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        let formatted = elementName.trimmingCharacters(in: .whitespacesAndNewlines)
        guard !formatted.isEmpty else { return }
        
        if formatted == "item" {
            inItem = true
            
            postTitle = ""
            postDescription = ""
            postDate = Date()
            postLink = ""
            
            return
        }
        
        guard inItem else { return }
        
        isTitle = formatted == "title"
        isDescription = formatted == "description"
        isDate = formatted == "pubDate"
        isLink = formatted == "link"
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        let formatted = elementName.trimmingCharacters(in: .whitespacesAndNewlines)
        guard !formatted.isEmpty else { return }
        guard inItem else { return }
        
        if formatted == "item" {
            inItem = false
            
            result.append(.init(link: postLink, title: postTitle, description: postDescription, date: postDate))
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let formatted = string.trimmingCharacters(in: .whitespacesAndNewlines)
        guard !formatted.isEmpty else { return }
        
        if isTitle {
            postTitle += formatted
        } else if isDescription {
            postDescription += formatted
        } else if isLink {
            postLink += formatted
        } else if isDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
            postDate = dateFormatter.date(from: formatted)!
        }
    }
    
}

