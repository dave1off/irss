import Foundation

final class RefreshSourceWithPostsUseCase: IRefreshSourceWithPostsUseCase {
    
    let refreshSourceWithPostsRepository: IRefreshSourceWithPostsRepository
    
    init(refreshSourceWithPostsRepository: IRefreshSourceWithPostsRepository) {
        self.refreshSourceWithPostsRepository = refreshSourceWithPostsRepository
    }
    
    func exec(request: DTO.Request, completion: @escaping (DTO.Response) -> Void) {
        let source = request.source
        
        let requestPosts = source.posts.map {
            DomainDataDTOs.RefreshSourceWithPostsRepository.Request.Post(
                link: $0.link, title: $0.title, description: $0.description, date: $0.date, isRead: $0.isRead
            )
        }
        
        let requestSource = DomainDataDTOs.RefreshSourceWithPostsRepository.Request.Source(
            title: source.title, link: source.link, posts: requestPosts
        )
        
        refreshSourceWithPostsRepository.exec(request: .init(source: requestSource)) { response in
            completion(.init(result: response.result))
        }
    }
    
}
