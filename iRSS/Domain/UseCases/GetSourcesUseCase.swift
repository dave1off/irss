import Foundation

final class GetSourcesUseCase: IGetSourcesUseCase {
    
    typealias GetSources = PresentationDomainDTOs.GetSourcesUseCase
    
    private let getSourcesRepository: IGetSourcesRepository
    
    init(getSourcesRepository: IGetSourcesRepository) {
        self.getSourcesRepository = getSourcesRepository
    }
    
    func exec(request: GetSources.Request, completion: @escaping (GetSources.Response) -> Void) {
        getSourcesRepository.exec(request: .init()) { response in
            switch response.result {
            case .success(let items):
                let sources = items.map { PresentationDomainDTOs.GetSourcesUseCase.Response.Item(title: $0.title, link: $0.link) }
                completion(.init(result: .success(sources)))
            case .failure(let error):
                completion(.init(result: .failure(error)))
            }
        }
    }
    
}
