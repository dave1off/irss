import Foundation

final class GetPostsFromDBUseCase: IGetPostsFromDBUseCase {
    
    private let getPostsFromDBRepository: IGetPostsFromDBRepository
    
    init(getPostsFromDBRepository: IGetPostsFromDBRepository) {
        self.getPostsFromDBRepository = getPostsFromDBRepository
    }
    
    func exec(request: DTO.Request, completion: @escaping (DTO.Response) -> Void) {
        getPostsFromDBRepository.exec(request: .init(source: .init(link: request.source.link))) { response in
            guard case .success(let dbPosts) = response.result else { return }
            
            let posts = dbPosts.map {
                DTO.Response.Post(link: $0.link, title: $0.title, description: $0.description, date: $0.date, isRead: $0.isRead)
            }
            
            completion(.init(result: .success(posts)))
        }
    }
    
}
