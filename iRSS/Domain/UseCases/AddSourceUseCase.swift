import Foundation

final class AddSourceUseCase: IAddSourceUseCase {
    
    typealias AddSource = PresentationDomainDTOs.AddSourceUseCase
    
    private let addSourceRepository: IAddSourceRepository
    
    init(addSourceRepository: IAddSourceRepository) {
        self.addSourceRepository = addSourceRepository
    }
    
    func exec(input: AddSource.Request, completion: @escaping (AddSource.Response) -> Void) {
        addSourceRepository.exec(request: .init(title: input.title, link: input.link)) { response in
            switch response.result {
            case .success:
                completion(.init(error: nil))
            case .failure(let error):
                completion(.init(error: error))
            }
        }
    }
    
}
