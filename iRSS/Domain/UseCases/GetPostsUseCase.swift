import Foundation

final class GetPostsUseCase: NSObject, IGetPostsUseCase {
    
    private let getPostsFromDBUseCase: IGetPostsFromDBUseCase
    private let getPostsFromNetworkUseCase: IGetPostsFromNetworkUseCase
    private let refreshSourceWithPostsUseCase: IRefreshSourceWithPostsUseCase
    
    private let dispatchGroup = DispatchGroup()
    
    init(
        getPostsFromDBUseCase: IGetPostsFromDBUseCase,
        getPostsFromNetworkUseCase: IGetPostsFromNetworkUseCase,
        refreshSourceWithPostsUseCase: IRefreshSourceWithPostsUseCase
    ) {
        self.getPostsFromDBUseCase = getPostsFromDBUseCase
        self.getPostsFromNetworkUseCase = getPostsFromNetworkUseCase
        self.refreshSourceWithPostsUseCase = refreshSourceWithPostsUseCase
    }
    
    func exec(request: DTO.Request, completion: @escaping (DTO.Response) -> Void) {
        var dbPosts: [PresentationDomainDTOs.GetPostsFromDBUseCase.Response.Post] = []
        var networkPosts: [PresentationDomainDTOs.GetPostsFromNetworkUseCase.Response.Post] = []
        
        dispatchGroup.enter()
        getPostsFromDBUseCase.exec(request: .init(source: .init(link: request.source.link))) { [weak self] response in
            guard let self = self else { return }
            guard case .success(let posts) = response.result else { return }
            
            dbPosts = posts
            self.dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        getPostsFromNetworkUseCase.exec(request: .init(source: .init(link: request.source.link))) { [weak self] response in
            guard let self = self else { return }
            guard case .success(let posts) = response.result else { return }
            
            networkPosts = posts
            self.dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) { [weak self] in
            let readPosts = dbPosts.filter { $0.isRead }
            let readSet = Set(readPosts.map { $0.link })
            
            var result: [DTO.Response.Post] = []
            
            if networkPosts.isEmpty {
                result = dbPosts.map {
                    DTO.Response.Post(link: $0.link, title: $0.title, description: $0.description, date: $0.date, isRead: $0.isRead)
                }
            } else {
                for post in networkPosts {
                    let isRead = readSet.contains(post.link)
                    result.append(.init(link: post.link, title: post.title, description: post.description, date: post.date, isRead: isRead))
                }
            }
            
            let savedPosts = result.map { PresentationDomainDTOs.RefreshSourceWithPostsUseCase.Request.Post(
                    link: $0.link, title: $0.title, description: $0.description, date: $0.date, isRead: $0.isRead
                )
            }
            
            let savedSource = PresentationDomainDTOs.RefreshSourceWithPostsUseCase.Request.Source(
                title: request.source.title, link: request.source.link, posts: savedPosts
            )
            
            self?.refreshSourceWithPostsUseCase.exec(request: .init(source: savedSource)) { response in
                completion(.init(result: .success(result)))
            }
        }
    }

}
