final class UpdateSourcesObserver {
    
    typealias OnUpdate = ((PresentationDomainDTOs.GetSourcesUseCase.Response) -> ())
    
    private let updateSourcesObservable: ISourcesObservable
    
    var onUpdate: OnUpdate?
    
    init(updateSourcesObservable: ISourcesObservable) {
        self.updateSourcesObservable = updateSourcesObservable
        
        updateSourcesObservable.addUpdateSourcesObserver(request: .init(observer: self))
    }
    
}

extension UpdateSourcesObserver: IUpdateSourcesObserver {
    
    func updateSources(response: DomainDataDTOs.UpdateSources.Response) {
        let sources = response.result.map { PresentationDomainDTOs.GetSourcesUseCase.Response.Item(title: $0.title, link: $0.link) }
        
        onUpdate?(.init(result: .success(sources)))
    }
    
}
