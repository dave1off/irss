import Foundation

struct PresentationDomainDTOs {
    
    struct AddSourceUseCase {
        
        struct Request {
            
            let title: String
            let link: String
            
        }
        
        struct Response {
            
            let error: Error?
            
        }
        
    }
    
    struct GetSourcesUseCase {
        
        struct Request { }
        
        struct Response {
            
            struct Item {
                
                let title: String
                let link: String
                
            }
            
            let result: Result<[Item]>
            
        }
        
    }
    
    struct GetPostsUseCase {
        
        struct Request {
            
            struct Source {
                
                let title: String
                let link: String
                
            }
            
            let source: Source
            
        }
        
        struct Response {
            
            struct Post {
                
                let link: String
                let title: String
                let description: String
                let date: Date
                let isRead: Bool
                
            }
            
            let result: Result<[Post]>
            
        }
        
    }
    
    struct RefreshSourceWithPostsUseCase {
        
        struct Request {
            
            struct Source {
                
                let title: String
                let link: String
                let posts: [Post]
                
            }
            
            struct Post {
                
                let link: String
                let title: String
                let description: String
                let date: Date
                let isRead: Bool
                
            }
            
            let source: Source
            
        }
        
        struct Response {
            
            let result: Result<Void>
            
        }
        
    }
    
    struct GetPostsFromNetworkUseCase {
        
        struct Request {
            
            struct Source {
                
                let link: String
                
            }
            
            let source: Source
            
        }
        
        struct Response {
            
            struct Post {
                
                let link: String
                let title: String
                let description: String
                let date: Date
                
            }
            
            let result: Result<[Post]>
            
        }
        
    }
    
    struct GetPostsFromDBUseCase {
        
        struct Request {
            
            struct Source {
                
                let link: String
                
            }
            
            let source: Source
            
        }
        
        struct Response {
            
            struct Post {
                
                let link: String
                let title: String
                let description: String
                let date: Date
                let isRead: Bool
                
            }
            
            let result: Result<[Post]>
            
        }
        
    }
    
    struct ReadPostUseCase {
        
        struct Request {
            
            struct Post {
                
                let link: String
                
            }
            
            let post: Post
            
        }
        
        struct Response {
            
            let result: Result<Void>
            
        }
        
    }
    
}
