import Foundation

protocol IReadPostUseCase {
    
    typealias DTO = PresentationDomainDTOs.ReadPostUseCase
    
    func exec(request: DTO.Request, completion: @escaping (DTO.Response) -> Void)
    
}
