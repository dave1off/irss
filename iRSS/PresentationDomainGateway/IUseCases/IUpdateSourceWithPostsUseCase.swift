import Foundation

protocol IRefreshSourceWithPostsUseCase {
    
    typealias DTO = PresentationDomainDTOs.RefreshSourceWithPostsUseCase
    
    func exec(request: DTO.Request, completion: @escaping (DTO.Response) -> Void)
    
}
