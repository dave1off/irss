import Foundation

protocol IGetPostsUseCase {
    
    typealias DTO = PresentationDomainDTOs.GetPostsUseCase
    
    func exec(request: DTO.Request, completion: @escaping (DTO.Response) -> Void)
    
}
