import Foundation

protocol IGetPostsFromDBUseCase {
    
    typealias DTO = PresentationDomainDTOs.GetPostsFromDBUseCase
    
    func exec(request: DTO.Request, completion: @escaping (DTO.Response) -> Void)
    
}
