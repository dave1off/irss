import Foundation

protocol IAddSourceUseCase {
    
    typealias DTO = PresentationDomainDTOs.AddSourceUseCase
    
    func exec(input: DTO.Request, completion: @escaping (DTO.Response) -> Void)
    
}
