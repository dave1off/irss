import Foundation

protocol IGetSourcesUseCase {
    
    typealias DTO = PresentationDomainDTOs.GetSourcesUseCase
    
    func exec(request: DTO.Request, completion: @escaping (DTO.Response) -> Void)
    
}
