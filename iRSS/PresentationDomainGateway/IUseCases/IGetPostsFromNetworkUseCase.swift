import Foundation

protocol IGetPostsFromNetworkUseCase {
    
    typealias DTO = PresentationDomainDTOs.GetPostsFromNetworkUseCase
    
    func exec(request: DTO.Request, completion: @escaping (DTO.Response) -> Void)
    
}
